const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes');
const cors = require('cors');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const connectedUsers = {};

io.on('connect', socket =>{
    const { user } = socket.handshake.query;

    //console.log(user, socket.id);

    //relacionando usuario com o id do socket
    connectedUsers[user] = socket.id;
}); 

mongoose.connect('mongodb+srv://semana:semana@cluster0-yq9jn.mongodb.net/omnistack8?retryWrites=true&w=majority', {
    useNewUrlParser: true
});

//da acesso ao controller dos dados qual usuario utiliza qual socket
app.use((req, res, next)=>{
    req.io = io;
    req.connectedUsers = connectedUsers;

    return next();
})


app.use(cors());

app.use(express.json());

app.use(routes);

server.listen(3333);