const Dev = require('../models/Dev');

module.exports={
    async store(req, res){

        const { user } = req.headers;
        const { idDev } = req.params;

        const loggedDev = await Dev.findById(user);
        const targetDev = await Dev.findById(idDev);

        if(!targetDev){
            return res.status(400).json({error: 'Dev not exists'});
        }

        if(targetDev.likes.includes(loggedDev._id)){
            const loggedSocket = req.connectedUsers[user];
            const targetSocket = req.connectedUsers[idDev];

            if(loggedSocket){
                req.io.to(loggedSocket).emit('match', targetDev);
				console.log('target'+targetDev);
            }

            if(targetSocket){
                req.io.to(targetSocket).emit('match', loggedDev);
				console.log('logged'+loggedDev);
            }
        }

        loggedDev.likes.push(targetDev._id);

        await loggedDev.save();

        return res.json(loggedDev);
    }
};